window.GapoScript = (() => {
  const utils = {
    isMobile: (agent) =>
      /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(
        agent || window.navigator.userAgent
      ),
    fixIE: () => {
      if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
        const msViewportStyle = document.createElement("style");
        msViewportStyle.appendChild(
          document.createTextNode("@-ms-viewport{width:auto!important}")
        );
        document.querySelector("head").appendChild(msViewportStyle);
      }
    },
  };

  const initSearchBox = () => {
    const $searchInput = $("#js-search-input");
    const $searchBox = $("#js-search-box");

    $searchInput
      .on("focus", () => {
        $searchBox.addClass("header__search-box--focus");
      })
      .on("blur", () => {
        $searchBox.removeClass("header__search-box--focus");
      });
  };
  const handleDarkMode = () => {
    const $DarkMode = $("#js-dark-mode");
    $DarkMode.on("click", () => {
      $("html").toggleClass("dark-mode");
    });
  };

  // const initSliderCustom = () => {
  //   let slideIndex = 1;
  //   // Next/previous controls

  //   // Thumbnail image controls

  // }
  // let slideIndex = 1;
  // const plusSlides = (n) => {
  //   showSlides((slideIndex += n));
  // }
  // const currentSlide = (n) => {
  //   showSlides((slideIndex = n));
  // }
  // const showSlides = (n) => {
  //   let i;
  //   let slides = $(".item");
  //   let dots = $(".dot");
  //   // let slideIndex = 1;
  //   // slideIndex++;
  //   if (n > slides.length) {
  //     slideIndex = 1;
  //   }
  //   if (n < 1) {
  //     slideIndex = slides.length;
  //   }
  //   for (i = 0; i < slides.length; i++) {
  //     slides[i].className = "item";
  //     dots[i].className = "dot";
  //   }

  //   if (slideIndex === 1) {
  //     slides[slides.length - 1].className = `item item-active item-prev`;
  //     slides[slideIndex - 1].className = `item item-active item-center`;
  //     slides[slideIndex].className = `item item-active item-next`;
  //   }
  //   if (slideIndex === slides.length) {
  //     slides[slideIndex - 2].className = `item item-active item-prev`;
  //     slides[slideIndex - 1].className = `item item-active item-center`;
  //     slides[0].className = `item item-active item-next`;
  //   }
  //   if (slideIndex !== slides.length && slideIndex !== 1) {
  //     slides[slideIndex - 2].className = `item item-active item-prev`;
  //     slides[slideIndex - 1].className = `item item-active item-center`;
  //     slides[slideIndex].className = `item item-active item-next`;
  //   }

  //   dots[slideIndex - 1].className += " active";
  //   // setTimeout(showSlides(slideIndex + 1), 3000);
  // }

  const init = () => {
    /* Fix IE */
    utils.fixIE();

    initSearchBox();
    handleDarkMode();
    // showSlides();
  };

  return {
    init,
  };
})();

$(document).ready(() => {
  window.GapoScript.init();
});

$(document).ready(() => {
  $(window).bind("scroll", function () {
    if ($(window).scrollTop() > 175) {
      $(".gametv-navbar").addClass("fixed");
    } else {
      $(".gametv-navbar").removeClass("fixed");
    }
  });
});
var slideIndex = 1;
showSlides(slideIndex);

// Next/previous controls
function plusSlides(n) {
  clearRunSlide();
  showSlides((slideIndex += n));
}

// Thumbnail image controls
function currentSlide(n) {
  clearRunSlide();
  showSlides((slideIndex = n));
}

function showSlides(n) {
  var i;
  var slides = $(".item");
  var dots = $(".dot");
  // slideIndex++;
  if (n > slides.length || slideIndex > slides.length) {
    slideIndex = 1;
  }
  if (n < 1 || slideIndex < 1) {
    slideIndex = slides.length;
  }
  for (i = 0; i < slides.length; i++) {
    slides[i].className = "item";
    dots[i].className = "dot";
  }
  if (slideIndex === 1) {
    slides[slides.length - 1].className = `item item-active item-prev`;
    slides[slideIndex - 1].className = `item item-active item-center`;
    slides[slideIndex].className = `item item-active item-next`;
  }
  if (slideIndex === slides.length) {
    slides[slideIndex - 2].className = `item item-active item-prev`;
    slides[slideIndex - 1].className = `item item-active item-center`;
    slides[0].className = `item item-active item-next`;
  }
  if (slideIndex !== slides.length && slideIndex !== 1) {
    slides[slideIndex - 2].className = `item item-active item-prev`;
    slides[slideIndex - 1].className = `item item-active item-center`;
    slides[slideIndex].className = `item item-active item-next`;
  }

  dots[slideIndex - 1].className += " active";
  autoRunSlide();
  // setTimeout(showSlides(slideIndex + 1), 3000);
}

var myVar;

function autoRunSlide() {
  myVar = setTimeout(function () {
    var newSlideIndex = slideIndex++;
    showSlides(newSlideIndex);
  }, 5000);
}

function clearRunSlide() {
  clearTimeout(myVar);
}
